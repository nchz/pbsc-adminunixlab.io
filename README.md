# Universidad Nacional Autónoma de México
# Dirección General de Cómputo y Tecnologías de Información y Comunicación
# UNAM-CERT
# Plan de Becarios de Seguridad en Cómputo
# Administración y Seguridad en Linux y UNIX

## Proyecto final

Instalar una infraestructura distribuida en varios proveedores de nube pública que contemple distintos serviciosde red en servidores Linux conectados a través de una VPN.

![Diagrama de conexión de servicios](img/diagrama.png "Diagrama de conexión de servicios")

### Condiciones

+ No cambiar la contraseña de `root`
+ No quitar las llaves existentes en `~/.ssh/authorized_keys`
  * Utilizar `~/.ssh/authorized_keys2`
  * Cambiar permisos con `chmod -c 0600 ~/.ssh/authorized_keys*`
+ Crear un usuario normal `becarios` y habilitar `sudo` sin contraseña con `NOPASSWD`
  * De preferencia agregar al usuario a un grupo privilegiado
+ No quitar ni modificar usuarios existentes
+ Tener cuidado con las reglas de `iptables`
  * Utilizar `iptables-apply` para evitar cerrar la puerta
+ Tener cuidado en la configuración del servicio `ssh`
+ Permitir **siempre** las conexiones de los siguientes equipos
  * `becarios.tonejito.info`
  * `becarios.priv.tonejito.info`
  * `10.0.8.0/24` (revisar esto con el equipo de VPN)
  * Direcciones IP de las máquinas de los otros equipos
  * Rangos IP de RedUNAM `132.247.0.0/16` y `132.248.0.0/16`

### Equipos

#### Database

+ Host	`database.becarios.tonejito.info`
+ IP	`104.198.7.77`
+ OS	CentOS 7
+ Documentación: <https://gitlab.com/PBSC-AdminUNIX/2017/database>

+ Instalar el servidor de *MySQL v5.7* y *PostgreSQL v9.6*
  * Desde el repositorio oficial utilizando paquetes
    - <https://dev.mysql.com/downloads/repo/yum/>
    - <https://dev.mysql.com/doc/mysql-yum-repo-quick-guide/en/>
    - <https://yum.postgresql.org/>
    - <https://yum.postgresql.org/repopackages.php>
    - <https://wiki.postgresql.org/wiki/YUM_Installation>
  * Este equipo tendrá el servidor `master` de MySQL y tendrá que coordinarse con el equipo **storage** para que configuren la réplica
+ Instalar *phpMyAdmin* y *phpPgAdmin*
  * No importa si es desde `.tar.gz`, paquetes o con `git`
  * No importa si PHP se configura como módulo de Apache o como FPM
+ Instalar *OpenVPN* desde paquetes
+ Este equipo tendrá la CA subordinada que firmará los certificados de cliente para la VPN
* Les ayudará bastante configurar el repositorio de *EPEL*

--------

#### Directory

+ Host	`directory.becarios.tonejito.info`
+ IP	`69.87.218.104`
+ OS	Debian 7
+ Documentación: <https://gitlab.com/PBSC-AdminUNIX/2017/directory>

+ Instalar *OpenLDAP* desde paquetes
+ Instalar *LDAP Account Manager* y *LDAP Toolbox Self Service*
  * No importa si es desde `.tar.gz`, paquetes o con `git`
  * No importa si PHP se configura como módulo de Apache o como FPM

--------

#### Mail

+ Host	`mail.becarios.tonejito.info`
+ IP	`45.77.165.190`
+ OS	CentOS 6
+ Documentación: <https://gitlab.com/PBSC-AdminUNIX/2017/mail>

+ Instalar *Postfix* y *Dovecot* desde paquetes
  * Deben autenticar a los usuarios desde **LDAP**
  * Los buzones de correo se deben guardar en el servidor **NFS**
    - El equipo **storage** debería exportar el directorio `/srv/home`
    - El equipo **directory** debería configurar el directorio `/srv/home/%u` como el *home* de cada usuario de **LDAP**
+ Abrir los puertos 25/tcp (smtp), 465/tcp (smtps) y 587/tcp (submission)
+ Abrir el puerto 993/tcp (imaps)
+ Permitir el relay desde las direcciones IP públicas y privadas de todos los demás equipos
+ Instalar *SquirrelMail* y configurar para enviar correo
  * No importa si es desde `.tar.gz`, paquetes o con `git`
  * No importa si PHP se configura como módulo de Apache o como FPM
* Les ayudará bastante configurar el repositorio de *EPEL*
+ Debe configurarse la conexión a **LDAP** para que se reconozcan los usuarios

--------

#### Storage

+ Host	`storage.becarios.tonejito.info`
+ IP	`208.113.129.109`
+ OS	Ubuntu 16.04 LTS
+ Documentación: <https://gitlab.com/PBSC-AdminUNIX/2017/storage>

+ Instalar y configurar el servidor de *NFS* v4
  * Exportar el directorio `/srv/home` para el equipo **mail**
  * Exportar algún directorio para que el equipo **web** pueda guardar los _htdocs_ de `drupal` y `wordpress`
+ Usuarios de NFS
  * Debe configurarse la conexión a **LDAP** para que se guarden adecuadamente los buzones de correo
  * Configurar un usuario local para que guarde los htdocs de `drupal` y `wordpress`
+ Instalar y configurar la replica de *MySQL* v5.7
  * Pueden utilizar el paquete que da el sistema operativo o el repositorio oficial de MySQL
  * Apoyarse en el equipo **database** para que les indiquen que hacer para empezar a replicar la base de datos
+ Instalar y configurar **BackupPC** desde paquetes
  * Utilizar `backuppc` para **copiar** los respaldos de otros equipos
    - Directorio LDAP (archivo ldif) generado por el equipo **directory**
    - Respaldos en formato SQL de MySQL y PostgreSQL generado por el equipo **database**

--------

#### Web

+ Host	`web.becarios.tonejito.info`
+ IP	`165.227.27.227`
+ OS	Debian 8
+ Documentación: <https://gitlab.com/PBSC-AdminUNIX/2017/web>

+ Instalar *Apache 2.4* desde paquetes
+ Instalar *PHP 7.0 FPM* desde el repositorio **DotDeb**
  * <https://www.dotdeb.org/instructions/>
  * Configurar el handler FastCGI de Apache
+ Instalar *Drupal* y *WordPress*
  * No importa si es desde `.tar.gz`, paquetes o con `git`
  * Drupal debe conectarse a la base de datos *PostgreSQL* en el equipo **database**
  * WordPress debe conectarse a la base de datos *MySQL* en el equipo **database**
+ Instalar *RoundCube*
  * No importa si es desde `.tar.gz`, paquetes o con `git`
  * Configurarlo para que se conecte a Postfix y Dovecot del equipo `mail`

--------

